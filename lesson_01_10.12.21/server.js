// console.log(process.env.NODE_ENV);

// [].reduce((acc, elem, idx, arr)=>{
// возвращает измененный аккумулятор
// }, начальное значение аккумулятора)

// ''
// []
// {}
// 100
// 0
// 'pizza'
// true

// let arr = [10, 20, 30, 40];

// let result = arr.reduce((total, number) => {
//     return total + number;
// }, 0);

// let result = arr.reduce((total, number) => total + number, 0);

// console.log(result);

let status = "Moderator";

// function checkStatus(status) {
//     if (status === "Moderator") {
//         console.log("Hello Moderator");
//     } else if (status === "Admin") {
//         console.log("Hello Admin");
//     } else {
//         console.log("Hello Guest");
//     }
// }

// function checkStatus(status) {
//     if (status === "Moderator") {
//         return "Hello Moderator";
//     }

//     if (status === "Admin") {
//         return "Hello Admin";
//     }

//     return "Hello Guest";
// }

function checkStatus(status) {
    if (status === "Moderator") {
        return "Hello Moderator";
    }

    return "Hello Guest";
}
